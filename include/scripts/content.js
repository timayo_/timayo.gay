// Nav
class nav extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML =`
            <h3 class=window-title><img src="https://www.timayo.gay/include/assets/icons/Roadsign.ico">navigation</h3>
            <div class=window id=nav>
                <ul>
                    <li><a href="/" title="home">home</a></li>
                    <li><a href="/profile.html" title="profile">profile</a></li>
                    <li><a href="/library.html" title="library">library</a></li>
                    <li><a href="/archive" title="archive">archive</a></li>
                    <ul>
                        <li>coming soon!</li>
                    </ul>
                    <li><a href="/about.html" title="about">about site</a></li>
                </ul>
            </div>
        `;
    }
}

// Webring
class webring extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML =`
        <div id="webring">
            <a href="https://vaporeonline.neocities.org" title="BlueJay" alt="Previous site"><== blueJay</a>
            <p>berry webring</p>
            <a href="https://pinhead.dev" title="pinhead" alt="Next site">pinhead ==></a>
        </div>
        `;
    }
}



// Define
customElements.define("component-nav", nav);
customElements.define("component-webring", webring);